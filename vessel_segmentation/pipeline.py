import argparse
import logging
import os
from collections import OrderedDict
from os.path import join, isfile
from typing import Sequence, Tuple

import nibabel as nib
import numpy as np
import pandas as pd

from vessel_segmentation import ARTERIAL_MARKER_LIST, ARTERIAL_MARKER_DICT
from vessel_segmentation.Timer import Timer
from vessel_segmentation.arterial_vessel import (
    analyze_skeleton,
    compute_vascular_distance,
    diameter_transform,
    generate_frequency_path,
    find_skeleton_paths,
    find_start_point,
    find_true_skeleton,
    largest_structure,
    find_longest_path,
    generate_orders_map,
    reduce_wb_analysis,
    find_main_trunk,
    compute_distance_wise_diameter,
)
from vessel_segmentation.postprocessing import (
    centerline_analysis,
)
from vessel_segmentation.prediction import neuronal_network_combination
from vessel_segmentation.tof_master import (
    centerline_transform,
    create_willis_cube,
    find_willis_center,
    hysteresis_thresholding_cube,
    hysteresis_thresholding_brain,
    mask_image,
    resample,
    extend_markers,
    extract_vessels_ved,
    correct_watershed,
    apply_transform,
    autobox_image,
)


def extract_centerline_and_diameter(
    tof: nib.Nifti1Image,
    markers_bin: np.ndarray,
    prefix: str,
    output: str,
    brain_vasculature: str = "CircleOfWillis",
) -> Tuple[np.ndarray, np.ndarray]:
    """Extract center line and diameter from markers mask.

    Args:
        tof (nib.Nifti1Image): Time of flight image.
        markers_bin (np.ndarray): Markers result of the segmentation.
        prefix (str): File name prefix.
        output (str): Output directory.
        brain_vasculature (str, optional): Defaults to "CircleOfWillis".

    Returns:
        Tuple[np.ndarray, np.ndarray]: center line and diameter images.
    """
    logging.info("4. ===> Arterial Centerline & Diameter <===")
    logging.info("   4.1 ++++ : Extracting Centerline")
    center_line = centerline_transform(markers_bin)
    nib.Nifti1Image(center_line, tof.affine, tof.header).to_filename(
        join(output, f"{prefix}_{brain_vasculature}_centerline.nii.gz")
    )

    logging.info("   4.2 ++++ : Extracting Diameter")
    diameter = diameter_transform(markers_bin, tof.header.get_zooms())

    nib.Nifti1Image(diameter, tof.affine, tof.header).to_filename(
        join(output, f"{prefix}_{brain_vasculature}_diameter.nii.gz")
    )

    return center_line, diameter


def skip_preprocessing_pipeline(
    tof_path: str,
    cube_path: str,
    mask_path: str,
    markers_path: str,
    prefix: str,
    args: argparse.Namespace,
    output: str,
    option: int = 1,
    labels_path: str = None,
    vessels_path: str = None,
):
    """Function to skip the preprocessing and generate metrics from command line inputs file.

    Args:
        tof_path (str): Path of the time of flight image.
        cube_path (str): Path of the cube image.
        mask_path (str): Path of the mask image.
        markers_path (str): Path of the markers image.
        prefix (str): File name prefix.
        args (argparse.Namespace): Command line arguments.
        option (int): If option == 2, WB segmentation is computed,
        labels_path (str): Path to labels nifti file to use instead of computing one,
        vessels_path (str): Path leading to vessels binary image instead of computing one,
    """
    # You should provides TOF_resampled.nii.gz with TOF_resampled_markers.nii.gz

    if None in [tof_path, cube_path, mask_path, markers_path]:
        raise IOError(
            f"Missing files in this list [TOF, CUBE, MASK, MARKERS]. "
            f"Got {[tof_path, cube_path, mask_path, markers_path]}"
        )
    tof = nib.load(tof_path)
    cube_img = nib.load(cube_path)
    mask_img = nib.load(mask_path)
    markers = nib.load(markers_path).get_fdata("unchanged")

    assert tof.shape == markers.shape, "TOF and Markers shape mismatch"
    assert tof.shape == cube_img.shape, "TOF and Cube shape mismatch"
    assert tof.shape == mask_img.shape, "TOF and Mask shape mismatch"

    affine: np.ndarray = tof.affine
    header: nib.Nifti1Header = tof.header
    center, markers_data = find_willis_center(markers)

    centerline, diameter = extract_centerline_and_diameter(
        tof, markers_data, prefix, output
    )

    resample_file_scaled = join(output, f"{prefix}_resampled_scaled.nii.gz")
    resampled_array = tof.get_fdata("unchanged")
    resample_array_norm = resampled_array[np.where(markers_data != 0)]
    upper_quantile = np.quantile(resample_array_norm, 0.99)
    resampled_array /= upper_quantile

    nib.Nifti1Image(resampled_array, affine, header).to_filename(resample_file_scaled)

    if not args.simple_segmentation:
        compute_arterial_metrics(
            resampled_array,
            markers,
            centerline,
            diameter,
            center,
            affine,
            header,
            output,
            prefix,
        )

    if option == 2:
        mask_data = mask_img.get_fdata("unchanged")
        if labels_path is None:
            labels = vessels_flood_filling(
                markers,
                mask=mask_data,
                header=header,
                affine=affine,
                output=output,
                vessels_file_args=vessels_path,
                prefix=prefix,
                resample_file=resample_file_scaled,
            )
        else:
            labels = nib.load(labels_path).get_fdata("unchanged")

        center_line, diameter = extract_centerline_and_diameter(
            tof, labels, prefix, output, brain_vasculature="CerebralArteries"
        )

        if not args.simple_segmentation:
            compute_arterial_metrics_full(
                markers,
                resampled_array,
                labels,
                center_line,
                diameter,
                center,
                affine,
                header,
                args,
                output,
                prefix,
                brain_vasculature="CerebralArteries",
            )


def compute_arterial_metrics_full(
    markers_data: np.ndarray,
    resampled_data: np.array,
    vessels_labeled: np.ndarray,
    center_line: np.ndarray,
    diameter: np.ndarray,
    center: Sequence[float],
    affine: np.ndarray,
    header: nib.Nifti1Header,
    args,
    output: str,
    prefix: str,
    brain_vasculature: str = "CerebralArteries",
):
    """Compute arterial metrics for each artery in the brain.

    Args:
        markers_data (np.ndarray): Markers segmentation.
        resampled_data (np.array): TOF image.
        vessels_labeled (np.array): Image of the vessels with a tag.
        center_line (np.ndarray): Center line image of the vessels labeled.
        diameter (np.ndarray): Diameter image of the vessels labeled.
        center (Sequence[float]): Center of mass of the markers.
        affine (np.ndarray): Reference affine.
        header (nib.Nifti1Header): Reference header.
        args (argparse.Namespace): Arguments from command line
        prefix (str): Use as prefix of filename.
        brain_vasculature (str, optional): Defaults to "CerebralArteries".

    """

    logging.info("3. ===> Computing Arterial Metrics <===")

    dfs_reduced = []
    wb = join(output, f"{prefix}_{brain_vasculature}")
    os.makedirs(wb, exist_ok=True)

    for label in np.unique(markers_data)[1:]:
        if label > len(ARTERIAL_MARKER_LIST):
            logging.warning(f"Label {label} isn't an identified artery!")
            continue

        artery_prefix = ARTERIAL_MARKER_LIST[int(label)]

        if artery_prefix not in ["LACA1", "RACA1", "LMCA1", "RMCA1", "LPCA2", "RPCA2"]:
            continue

        path_prefix = f"{prefix}_{brain_vasculature}_{artery_prefix}"

        logging.info(f"++++ Starting {path_prefix} Artery Metrics ++++")

        roi = np.zeros_like(vessels_labeled)
        roi[np.where(vessels_labeled == label)] = 1

        roi_markers = np.zeros_like(markers_data)
        roi_markers[np.where(markers_data == label)] = 1

        logging.info("5.1 ++++ Largest Structure Correction ++++")
        with Timer():
            structure = largest_structure(roi)
            markers_structure = largest_structure(roi_markers)
            intensity_data = structure * resampled_data

        nib.Nifti1Image(structure, affine, header).to_filename(
            join(wb, f"{path_prefix}_largest.nii.gz")
        )

        nib.Nifti1Image(intensity_data, affine, header).to_filename(
            join(wb, f"{path_prefix}_intensity_scaled.nii.gz")
        )

        dfs_reduced = compute_summary_WB(
            args,
            wb,
            structure,
            center_line,
            markers_structure,
            path_prefix,
            affine,
            header,
            center,
            resampled_data,
            diameter,
            dfs_reduced,
            artery_prefix,
            comments="",
        )

    metrics = OrderedDict(dfs_reduced)
    pd.DataFrame(metrics).to_csv(join(wb, f"{prefix}_{brain_vasculature}_SUMMARY.csv"))


def compute_summary_WB(
    args: argparse.Namespace,
    output: str,
    structure: np.array,
    center_line: np.array,
    markers_structure: np.array,
    path_prefix: str,
    affine: np.array,
    header: nib.Nifti1Header,
    center: Sequence[float],
    resampled_data: np.array,
    diameter: np.array,
    dfs_reduced: list,
    artery_prefix: str,
    comments: str = "",
) -> list:
    """Compute a summary file for whole brain segmentation. Only work for ACA L-R, MCA L-R, PCA L-R

    Args:
        args (argparse.Namespace): Argument from the command line
        output (str): Output directory name
        structure (np.array): Current WB arterial struture
        center_line (np.array): Current centerline array
        markers_structure (np.array): Current CW arterial structure
        path_prefix (str): Saved file prefix name
        affine (np.array): Nifti affine matrix
        header (nib.Nifti1Header): Nifti header
        center (Sequence[float]): Center of mass of the markers
        resampled_data (np.array): Tof array normalized
        diameter (np.array): Diameter image
        dfs_reduced (list): List to append current arterial information
        artery_prefix (str): Name of the current artery
        comments (str): Comments

    Returns:
        dfs_reduced (list): List with appended current arterial information

    """
    logging.info("5.3 ++++ Analyzing Skeleton ++++")

    with Timer():
        skeleton = analyze_skeleton(structure * center_line)
        marker_skeleton = analyze_skeleton(markers_structure * center_line)
    nib.Nifti1Image(skeleton, affine, header).to_filename(
        join(output, f"{path_prefix}_analysed_skl{comments}.nii.gz")
    )

    # Determining If a single End point is found in the current artery
    unique, counts = np.unique(skeleton, return_counts=True)

    skl_dict = OrderedDict(zip(unique[1:], counts[1:]))
    if 9 not in skl_dict or (skl_dict[9] < 2 and 7 not in skl_dict):
        logging.info(f"Artery {path_prefix} has insufficient end tags for analysis")
        return None

    if skl_dict[9] == 1 and skl_dict[7] >= 1:
        logging.info(
            f"Artery {path_prefix} Has Only One End Tags and One Junction tags"
        )
        logging.info(" ===> Converting Junction Tag To End Tag")
        skeleton[skeleton == 7] = 9

    logging.info("5.4 ++++ Finding Willis Start Point ++++")

    with Timer():
        start = find_start_point(marker_skeleton, center)

    logging.info("5.5 ++++ Djikstra's Skeleton Paths Computation ++++")

    with Timer():
        costs, indices = find_skeleton_paths(start, skeleton)

    logging.info("5.6 ++++ Computing True Skeleton ++++")

    with Timer():
        true_skeleton = find_true_skeleton(indices, skeleton.shape)
    nib.Nifti1Image(true_skeleton, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_true{comments}.nii.gz")
    )

    logging.info("5.7 ++++ Computing Frequency Path ++++")

    with Timer():
        frequency = generate_frequency_path(indices, skeleton.shape)

    nib.Nifti1Image(frequency, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_frequency{comments}.nii.gz")
    )

    logging.info(
        "5.8 ++++ Computing Vascular Distance & Mask Distance to Vessel & Travelled ++++"
    )

    with Timer():
        (
            distance,
            tortuosity,
            _,
            _,
            _,
        ) = compute_vascular_distance(structure, indices, frequency, header.get_zooms())
    nib.Nifti1Image(distance, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_distance{comments}.nii.gz")
    )
    nib.Nifti1Image(tortuosity, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_tortuosity{comments}.nii.gz")
    )

    logging.info("5.9 ++++ Computing Main Trunk ++++")

    with Timer():
        main_trunk = find_main_trunk(
            frequency, start, distance=distance, main_trunk_stop=args.main_trunk_stop
        )

    nib.Nifti1Image(main_trunk, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_main{comments}.nii.gz")
    )

    logging.info("5.10 ++++ Computing Longest Path ++++")

    with Timer():
        longest_path = find_longest_path(
            indices=indices, skeleton_shape=skeleton.shape, distance=distance
        )

    nib.Nifti1Image(longest_path, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_longest{comments}.nii.gz")
    )

    logging.info("5.10.1 ++++ Computing Orders ++++")

    with Timer():
        orders = generate_orders_map(frequency, main_trunk, indices)

    nib.Nifti1Image(orders, affine, header).to_filename(
        join(output, f"{path_prefix}_skl_orders{comments}.nii.gz")
    )
    dataframe = reduce_wb_analysis(
        orders, distance, longest_path, indices, diameter.copy()
    )
    dfs_reduced.append((artery_prefix, dataframe))

    df_dict = {
        "Intensity": resampled_data.copy(),
        "Diameter": diameter.copy(),
        "Tortuosity": tortuosity.copy(),
    }

    for k, v in df_dict.items():
        logging.info("\n   +++: %s \n" % (k,))
        df = compute_distance_wise_diameter(longest_path, distance, v, args.step)
        df.to_csv(
            join(output, f"{path_prefix}_Distance_wise_{k}_longest_path{comments}.csv")
        )
        logging.info(df)

    return dfs_reduced


def full_pipeline(
    args: argparse.Namespace,
    prefix: str,
    option: int = 1,
    labels_path: str = None,
    vessels_path: str = None,
):
    """Compute the preprocessing, prediction, and metrics.

    Args:
        args (argparse.Namespace): Command line arguments.
        prefix (str): File name prefix.
        option (int): If option == 2, WB segmentation is computed,
        labels_path (str): Path to labels nifti file to use instead of computing one,
        vessels_path (str): Path leading to vessels binary image instead of computing one,
    """

    resolution_nn = (0.625, 0.625, 0.625)
    nn_resolution_path = join(args.output, "nn_space")
    os.makedirs(nn_resolution_path, exist_ok=True)
    original_path = join(args.output, "original_space")
    os.makedirs(original_path, exist_ok=True)

    template_path = join(args.template_path, f"AVG_TOF_MNI_SS_down.nii.gz")
    template_sphere_path = join(args.template_path, f"willis_sphere_down.nii.gz")
    tof_raw: nib.Nifti1Image = nib.load(args.TOF)
    logging.info(f"Original orientation: {nib.aff2axcodes(tof_raw.affine)}")

    tof = nib.as_closest_canonical(tof_raw)
    logging.info(f"Transformed orientation: {nib.aff2axcodes(tof.affine)}")

    reorient_file = join(original_path, f"{prefix}_reorient.nii.gz")
    tof.to_filename(reorient_file)

    autobox_file = join(original_path, f"{prefix}_autobox.nii.gz")
    autobox_image(reorient_file, autobox_file, pad=6)

    logging.info("1. ===> Preprocessing <===")
    logging.info("   1.2 ++++ : Resampling to Neural Network Resolution")

    resample_file = join(nn_resolution_path, f"{prefix}_resampled.nii.gz")
    nii_attributes = resample(autobox_file, resample_file, resolution=resolution_nn)
    resampled_array = nii_attributes.get_fdata("unchanged")

    logging.info("2.  ===> Willis Labelling <===")
    logging.info("   2.1 ++++ : Creating a Brain Mask of NN Resolution")

    if args.mask:
        logging.info(f"    Used parse mask: {args.mask}")
        mask_file = join(nn_resolution_path, f"{prefix}_mask.nii.gz")
        nii_attributes_mask = resample(
            args.mask, mask_file, master=resample_file, resample_mode="NN"
        )
        mask_array = nib.as_closest_canonical(nii_attributes_mask).get_fdata()
        brain_mask_array = mask_array * resampled_array
        brain_mask_file = join(nn_resolution_path, f"{prefix}_SS_Input.nii.gz")
        nib.Nifti1Image(
            brain_mask_array, nii_attributes.affine, nii_attributes.header
        ).to_filename(brain_mask_file)
        nib.Nifti1Image(
            mask_array, nii_attributes.affine, nii_attributes.header
        ).to_filename(mask_file)
    else:
        mask_file = join(nn_resolution_path, f"{prefix}_mask.nii.gz")
        brain_mask_file = join(
            nn_resolution_path, f"{prefix}_SS_RegistrationImage.nii.gz"
        )
        mask_image(resample_file, mask_file, brain_mask_file)

    logging.info("   2.2 ++++ : Finding Circle of Willis masking cube")

    cube_file = join(nn_resolution_path, f"{prefix}_cube.nii.gz")
    if args.cube is None:
        sphere, cube = create_willis_cube(
            brain_mask_file,
            nn_resolution_path,
            template_path,
            template_sphere_path,
        )
        nib.Nifti1Image(
            sphere, nii_attributes.affine, nii_attributes.header
        ).to_filename(join(nn_resolution_path, f"{prefix}_sphere.nii.gz"))
        nib.Nifti1Image(cube, nii_attributes.affine, nii_attributes.header).to_filename(
            cube_file
        )
    else:
        cube = resample(
            args.cube, cube_file, master=resample_file, resample_mode="NN"
        ).get_fdata("unchanged")

    logging.info("   2.3 ++++ : Predicting Circle of willis markers")

    if args.markers is None:

        markers_file = join(nn_resolution_path, f"{prefix}_markers.nii.gz")
        markers_file_for_lab = join(nn_resolution_path, f"{prefix}_mffl.nii.gz")

        voxel_volume: float = np.prod(resolution_nn)

        binary_image = hysteresis_thresholding_cube(
            resampled_array,
            cube,
            voxel_size=resolution_nn[0],
        )

        # nib.Nifti1Image(binary_image, nii_attributes.affine, nii_attributes.header).to_filename(
        #     join(args.output, f"{prefix}_bin_vessels.nii.gz")
        # )

        neuronal_network_combination(
            resampled_array,
            args,
            prefix,
            cube,
            binary_image,
            voxel_volume,
            nii_attributes,
            resample_file,
            markers_file,
            markers_file_for_lab,
            output_directory=nn_resolution_path,
            experimental_artery=args.experimental_prediction,
        )

    else:
        markers_file = args.markers

    metric_path = join(args.output, "metric_space")
    os.makedirs(metric_path, exist_ok=True)

    resolution = (args.resolution, args.resolution, args.resolution)
    logging.info("3. ===> Brain Masking & Resampling <===")

    logging.info("   3.1 ++++ : Resampling Autoboxed Image to Target Resolution.")
    resample_file_metric = join(args.output, f"{prefix}_resampled.nii.gz")
    nii_attributes = resample(autobox_file, resample_file_metric, resolution)

    logging.info(
        "   3.2 ++++ : Resampling Circle of Willis Markers to Target Resolution."
    )
    markers_resample_file = join(args.output, f"{prefix}_eICAB_CW.nii.gz")
    resample(
        markers_file,
        markers_resample_file,
        master=resample_file_metric,
        resample_mode="NN",
    )

    logging.info("   3.3 ++++ : Resampling mask to target resolution.")
    mask_resample_file = join(metric_path, f"{prefix}_mask.nii.gz")
    resample(
        mask_file, mask_resample_file, master=resample_file_metric, resample_mode="NN"
    )

    logging.info("   3.4 ++++ : Resampling cube to target resolution.")
    cube_resampled_file = join(metric_path, f"{prefix}_cube.nii.gz")
    resample(
        cube_file, cube_resampled_file, master=resample_file_metric, resample_mode="NN"
    )

    ss_mask = join(args.template_path, f"SSS_masked_down.nii.gz")

    if not (
        isfile(join(nn_resolution_path, "WILLIS_ANTS0GenericAffine.mat"))
        or isfile(join(metric_path, "SSS_masked_reg.nii.gz"))
    ):
        raise IOError(
            f"WILLIS_ANTS0GenericAffine.mat and/or SSS_masked_reg.nii.gz not in output folder -> {args.output}"
        )

    ss_mask_reg = apply_transform(
        ss_mask,
        resample_file_metric,
        join(nn_resolution_path, "WILLIS_ANTS0GenericAffine.mat"),
        join(metric_path, "SSS_masked_reg.nii.gz"),
    )

    mask_array = nib.load(mask_resample_file).get_fdata("unchanged")
    cube_resampled_array = nib.load(cube_resampled_file).get_fdata("unchanged")
    mask_cube = (
        (np.logical_or(mask_array, cube_resampled_array).astype(float) - ss_mask_reg)
        > 0
    ).astype(np.uint8)
    mask_cube_resample_file = join(
        metric_path, f"{prefix}_mask_cube_sss_resampled.nii.gz"
    )
    nib.Nifti1Image(
        mask_cube, nii_attributes.affine, nii_attributes.header
    ).to_filename(mask_cube_resample_file)

    skip_preprocessing_pipeline(
        resample_file_metric,
        cube_resampled_file,
        mask_cube_resample_file,
        markers_resample_file,
        prefix,
        args,
        output=metric_path,
        option=option,
        labels_path=labels_path,
        vessels_path=vessels_path,
    )


def compute_arterial_metrics(
    resample_array_norm: np.ndarray,
    markers_data: np.ndarray,
    center_line: np.ndarray,
    diameter: np.ndarray,
    center: Sequence[float],
    affine: np.ndarray,
    header: nib.Nifti1Header,
    output: str,
    prefix: str,
    brain_vasculature: str = "CircleOfWillis",
):
    """Compute simple metrics within the markers and output SUMMARY csv file

    Args:
        resample_array_norm (np.array): Tof normalized with CW high intensity,
        markers_data (np.ndarray): Markers segmentation.
        center_line (np.ndarray): Center line image of the markers
        diameter (np.ndarray): Diameter image of the markers
        center (Sequence[float]): Center of mass of the markers
        affine (np.ndarray): Reference affine.
        header (nib.Nifti1Header): Reference header.
        output (str): Define output directory.
        prefix (str): Use as prefix of filename.
        brain_vasculature (str, optional): Defaults to "CircleOfWillis".
    """

    logging.info("3. ===> Computing Arterial Metrics <===")

    cw = join(output, f"{prefix}_{brain_vasculature}")
    os.makedirs(cw, exist_ok=True)

    statistics = OrderedDict(
        (
            ("Volume", np.nan),
            ("Diameter mean", np.nan),
            ("Diameter median", np.nan),
            ("Diameter max", np.nan),
            ("Diameter min", np.nan),
            ("Diameter STD", np.nan),
            ("Diameter N", np.nan),
            # ("Intensity scaled AVG", np.nan),
            # ("Intensity scaled STD", np.nan),
            # ("Weighted average tortuosity", np.nan),
            # ("Total length", np.nan),
            # ("Longest path", np.nan),
            ("Confidence", "Bad"),
        )
    )

    metrics = OrderedDict(
        [
            (key, statistics.copy())
            for key, value in ARTERIAL_MARKER_DICT.items()
            if 0 < value <= 14 or (value in markers_data and value > 0)
        ]
    )
    center_line_diameter = center_line * diameter

    artery_flags = centerline_analysis(markers_data.copy(), center_line, step=1)

    for label in np.unique(markers_data)[1:]:
        if label > len(ARTERIAL_MARKER_LIST):
            logging.warning(f"Label {label} isn't an identified artery!")
            continue

        artery_prefix = ARTERIAL_MARKER_LIST[int(label)]
        path_prefix = f"{prefix}_{brain_vasculature}_{artery_prefix}"

        logging.info(f"++++ Starting {path_prefix} Artery Metrics ++++")

        roi = np.zeros_like(markers_data)
        roi[np.where(markers_data == label)] = 1

        metrics[artery_prefix]["Volume"] = np.count_nonzero(roi) * (
            np.mean(header.get_zooms()) ** 3
        )
        # metrics[artery_prefix]["Intensity scaled AVG"] = np.mean(
        #     resample_array_norm[np.where(roi != 0)]
        # )
        # metrics[artery_prefix]["Intensity scaled STD"] = np.std(
        #     resample_array_norm[np.where(roi != 0)]
        # )
        seg_diameter = center_line_diameter * roi
        seg_diameter: np.ndarray = seg_diameter[np.where(seg_diameter != 0)].flatten()

        logging.info("3.2 ++++ Diameter Extraction ++++")
        if seg_diameter.size != 0:
            logging.info("   ===> DIAMETERS:")
            metrics[artery_prefix]["Diameter mean"] = seg_diameter.mean()
            metrics[artery_prefix]["Diameter median"] = np.median(seg_diameter)
            metrics[artery_prefix]["Diameter max"] = seg_diameter.max()
            metrics[artery_prefix]["Diameter min"] = seg_diameter.min()
            metrics[artery_prefix]["Diameter STD"] = seg_diameter.std()
            metrics[artery_prefix]["Diameter N"] = seg_diameter.size
            metrics[artery_prefix]["Confidence"] = "Good"

        # logging.info("3.1 ++++ Largest Structure Correction ++++")
        # with Timer():
        #     structure = largest_structure(roi)
        # nib.Nifti1Image(structure, affine, header).to_filename(
        #     join(cw, f"{path_prefix}_largest.nii.gz")
        # )

        # logging.info("3.3 ++++ Analyzing Skeleton ++++")
        #
        # with Timer():
        #     skeleton = analyze_skeleton(roi * center_line)
        # nib.Nifti1Image(skeleton, affine, header).to_filename(
        #     join(cw, f"{path_prefix}_analysed_skl.nii.gz")
        # )

        # # Determining if a single end point is found in the current artery
        # unique, counts = np.unique(skeleton, return_counts=True)
        #
        # skl_dict = dict(zip(unique[1:], counts[1:]))
        # if 9 not in skl_dict or (skl_dict[9] < 2 and 7 not in skl_dict):
        #     logging.info(f"Artery {path_prefix} has insufficient end tags for analysis")
        #     metrics[artery_prefix]["Confidence"] = "Bad"
        #     continue
        #
        # if skl_dict[9] == 1 and skl_dict[7] >= 1:
        #     logging.info(
        #         f"Artery {path_prefix} Has Only One End Tags and One Junction tags"
        #     )
        #     logging.info("    ===> Converting Junction Tag To End Tag")
        #     skeleton[skeleton == 7] = 9

        # logging.info("3.4 ++++ Finding Willis Start Point ++++")

        # with Timer():
        #     if artery_prefix in ["LCAR", "RCAR"]:
        #         logging.info(" +++ ICA start")
        #         coord = np.nonzero(skeleton == 9)
        #         idx = np.argmin(coord[-1])
        #         start = np.array((coord[0][idx], coord[1][idx], coord[2][idx]))
        #
        #     else:
        #         start = find_start_point(skeleton, center)
        #
        # logging.info("3.5 ++++ Djikstra's Skeleton Paths Computation ++++")
        # with Timer():
        #     _, indices = find_skeleton_paths(start, skeleton)
        #
        # logging.info("3.6 ++++ Computing Frequency Path ++++")
        #
        # with Timer():
        #     frequency = generate_frequency_path(indices, skeleton.shape)
        #
        # nib.Nifti1Image(frequency, affine, header).to_filename(
        #     join(cw, f"{path_prefix}_skl_frequency.nii.gz")
        # )
        #
        # logging.info(
        #     "3.7 ++++ Computing Vascular Distance & Mask Distance to Vessel & Travelled ++++"
        # )
        # with Timer():
        #     (
        #         distance,
        #         tortuosity,
        #         _,
        #         corrected_length,
        #         average_tortuosity,
        #     ) = compute_vascular_distance(roi, indices, frequency, header.get_zooms())
        # nib.Nifti1Image(distance, affine, header).to_filename(
        #     join(cw, f"{path_prefix}_skl_distance.nii.gz")
        # )
        # nib.Nifti1Image(tortuosity, affine, header).to_filename(
        #     join(cw, f"{path_prefix}_skl_tortuosity.nii.gz")
        # )

        # logging.info("5.9 ++++ Distance Wise Table Computation ++++")
        # metrics[artery_prefix]["Weighted average tortuosity"] = average_tortuosity
        # metrics[artery_prefix]["Total length"] = corrected_length
        # metrics[artery_prefix]["Longest path"] = distance.max()

        if artery_prefix in artery_flags.keys():
            if artery_flags[artery_prefix]:
                metrics[artery_prefix]["Confidence"] = "Bad"

        elif metrics[artery_prefix]["Confidence"] != "Bad":
            metrics[artery_prefix]["Confidence"] = "Good"

    pd.DataFrame(metrics).to_csv(join(cw, f"{prefix}_SUMMARY.csv"))


def vessels_flood_filling(
    markers_data: np.array,
    mask: np.array,
    affine: np.array,
    header: nib.Nifti1Header,
    output: str,
    vessels_file_args: str,
    prefix: str,
    resample_file: str,
) -> Tuple[np.array, np.array]:
    """Watershed markers in the binary vessel image

    Args:
        markers_data (np.array): Circle of Willis.
        mask (np.array): Brain mask.
        affine (np.array): Affine matrix.
        header (nib.Nifti1Header): Header.
        output (str): Output directory.
        vessels_file_args (str): Vessels file.
        prefix (str): File name prefix.
        resample_file (str): TOF resampled.

    Returns:
        labels (np.array): Watershed image.
    """

    vesselness_file = join(output, f"{prefix}_vesselness.nii.gz")
    vessels_file = join(output, f"{prefix}_vessels_hysteresis.nii.gz")
    labels_file = join(output, f"{prefix}_labels.nii.gz")

    if vessels_file_args:
        vessels = nib.load(vessels_file_args).get_fdata("unchanged")

    else:
        vesselness = extract_vessels_ved(resample_file, vesselness_file, mask, output)
        nib.Nifti1Image(vesselness, affine, header).to_filename(vesselness_file)

        vessels = hysteresis_thresholding_brain(vesselness, mask)
        nib.Nifti1Image(vessels, affine, header).to_filename(vessels_file)

    labels = extend_markers(vessels, markers_data, connectivity=3, extension=False)
    labels = correct_watershed(markers_data, labels)
    nib.Nifti1Image(labels, affine, header).to_filename(labels_file)

    return labels
