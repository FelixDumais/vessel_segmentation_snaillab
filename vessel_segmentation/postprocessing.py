# -*- coding: utf-8 -*-
from typing import List

from vessel_segmentation.arterial_vessel import special_end_points

import logging

import numpy as np
from skimage.measure import label
from skimage.morphology import (
    binary_dilation,
    remove_small_holes,
)

from vessel_segmentation import ArterialEnum


def largest_structure(data: np.ndarray, arteries: List[int]) -> np.ndarray:
    """Find largest structure in a label image.

    Args:
        arteries:
        data (np.ndarray): Label input image.

    Returns:
        np.ndarray: array with the largest structure from the mask.
    """

    def largest_internal(markers, index):
        mask = markers == index
        markers[markers == index] = 0
        labeled = label(
            mask.astype(np.uint8),
            background=0,
            return_num=False,
            connectivity=3,
        )
        unique, counts = np.unique(labeled, return_counts=True)
        max_label = unique[np.argmax(counts[1:]) + 1]
        markers[labeled == max_label] = index

        return markers

    if len(arteries) > 0:
        logging.info("Largest specific")
        for idx in arteries:
            if idx in data:
                data = largest_internal(data, idx)

    else:
        logging.info("Largest all")
        for idx in np.unique(data)[1:]:
            data = largest_internal(data, idx)

    return data


def volume_analysis(data: np.ndarray, voxel_volume: float) -> np.ndarray:
    """Filter prediction given an expert rule.
    All the predicted class should have a volume over these threshold,
    cow = [
        (4, 1),
        (5, 10),
        (6, 10),
        (9, 1),
        (10, 1),
        (11, 5),
        (12, 5),
    ]

    Args:
        data (np.ndarray): Label input image.
        voxel_volume (float): voxel volume.

    Returns:
        np.ndarray: Filtered input
    """
    # All value correspond to 25% of the minimum volume per pair of artery.
    # If only one artery, we just take the min of that one.
    reference_volume = 0.625 ** 3
    image = data.copy()
    cow = [
        (ArterialEnum.RCAR, 90 * reference_volume),
        (ArterialEnum.LCAR, 90 * reference_volume),
        (ArterialEnum.BAS, 10 * reference_volume),
        # For AComm better results with 100 % min value
        (ArterialEnum.Acom, 4 * reference_volume),
        (ArterialEnum.LACA1, 20 * reference_volume),
        (ArterialEnum.RACA1, 20 * reference_volume),
        (ArterialEnum.LMCA1, 40 * reference_volume),
        (ArterialEnum.RMCA1, 40 * reference_volume),
        # For PComm better results with 100 % min value
        (ArterialEnum.LPcom, 12 * reference_volume),
        (ArterialEnum.RPcom, 12 * reference_volume),
        (ArterialEnum.LPCA1, 9 * reference_volume),
        (ArterialEnum.RPCA1, 9 * reference_volume),
        (ArterialEnum.LPCA2, 60 * reference_volume),
        (ArterialEnum.RPCA2, 60 * reference_volume),
        (ArterialEnum.LSCA, 60 * reference_volume),
        (ArterialEnum.RSCA, 60 * reference_volume),
        (ArterialEnum.LAChA, 7 * reference_volume),
        (ArterialEnum.RAChA, 7 * reference_volume),
    ]

    for idx, bound in cow:
        bin_image = data == idx.value
        labeled = label(
            bin_image.astype(np.uint8),
            background=0,
            return_num=False,
            connectivity=3,
        )
        for l in np.unique(labeled)[1:]:
            sub_bin = labeled == l
            volume = sub_bin.sum() * voxel_volume

            if volume <= bound:
                logging.info(f"Delete sublabel {idx}")
                image[sub_bin] = 0

    return image


def dilation(data: np.ndarray) -> np.ndarray:
    """Dilate each label of the prediction

    .. warning::
        The label could overlap if they are close together.

    Args:
        data (np.ndarray): Label input image.

    Returns:
        np.ndarray: Dilated label image.
    """
    image = np.zeros_like(data)

    for idx in np.unique(data)[1:]:
        unique_open = image == idx
        image_dilated = binary_dilation(unique_open)
        image[image_dilated] = idx

    return image


def remove_holes(data: np.ndarray) -> np.ndarray:
    """Remove holes from the label image.

    Args:
        data (np.ndarray): Label input image.

    Returns:
        np.ndarray: Label with holes removed.
    """
    image = np.zeros_like(data)
    for idx in np.unique(data)[1:]:
        unique_filled = data == idx
        unique_filled = remove_small_holes(unique_filled, connectivity=1)
        image[unique_filled] = idx

    return image


def aplasia_analysis(data: np.ndarray) -> np.ndarray:
    """Check if the Acom is there if the patient has aplasia. If so, the Acom is removed

    Args:
        data (np.ndarray): Label input image

    Returns:
        np.ndarray: Filtered label image
    """
    image = data.copy()

    anterior_config = np.unique(image)[1:]

    if ArterialEnum.Acom.value in anterior_config:
        if (
            ArterialEnum.LACA1.value not in anterior_config
            or ArterialEnum.RACA1.value not in anterior_config
        ):
            image[image == ArterialEnum.Acom.value] = 0

    return image


def centerline_analysis(markers: np.array, centerline: np.array, step: int = 1) -> dict:
    """Analysis of the centerline to verify that the prediction makes sense.
       For example, if the centerline of the PComm does not touch the PCA1 or the PCA2,
       and the ICA, a flag is raised.

       Patches are created arround end point of each artery segment. Step determine the size of those patches.
       If step == 1, patch.shape == (3, 3, 3), if step == 2, patch.shape == (5, 5, 5), etc.

    Args:
        markers (np.ndarray): Label input image
        centerline (np.ndarray): Centerline of the markers
        step (int): Size of the patches.

    Returns:
        artery_flags (dict): Dictionnary of flags
    """

    centerline = centerline.astype(bool).astype(np.float64)
    centerline_markers = centerline * np.round(markers)
    centerline_markers_present = np.unique(centerline_markers)
    markers = largest_structure(markers, arteries=[])
    markers_present = np.unique(markers)

    artery_checks = [
        (ArterialEnum.RPCA1, [(ArterialEnum.RPCA2,), (ArterialEnum.BAS,)]),
        (ArterialEnum.LPCA1, [(ArterialEnum.LPCA2,), (ArterialEnum.BAS,)]),
        (
            ArterialEnum.RPcom,
            [
                (ArterialEnum.RPCA1, ArterialEnum.RPCA2),
                (ArterialEnum.RCAR,),
            ],
        ),
        (
            ArterialEnum.LPcom,
            [
                (ArterialEnum.LPCA1, ArterialEnum.LPCA2),
                (ArterialEnum.LCAR,),
            ],
        ),
        (ArterialEnum.Acom, [(ArterialEnum.RACA1,), (ArterialEnum.LACA1,)]),
        (
            ArterialEnum.LACA1,
            [(ArterialEnum.LCAR, ArterialEnum.LMCA1)],
        ),
        (
            ArterialEnum.RACA1,
            [(ArterialEnum.RCAR, ArterialEnum.RMCA1)],
        ),
        (
            ArterialEnum.LCAR,
            [(ArterialEnum.LACA1, ArterialEnum.LMCA1)],
        ),
        (
            ArterialEnum.RCAR,
            [(ArterialEnum.RACA1, ArterialEnum.RMCA1)],
        ),
        (
            ArterialEnum.LMCA1,
            [(ArterialEnum.LCAR, ArterialEnum.LACA1)],
        ),
        (
            ArterialEnum.RMCA1,
            [(ArterialEnum.RCAR, ArterialEnum.RACA1)],
        ),
        (
            ArterialEnum.LPCA2,
            [(ArterialEnum.LPCA1, ArterialEnum.LPcom)],
        ),
        (
            ArterialEnum.RPCA2,
            [(ArterialEnum.RPCA1, ArterialEnum.RPcom)],
        ),
        (
            ArterialEnum.LSCA,
            [(ArterialEnum.BAS,)],
        ),
        (
            ArterialEnum.RSCA,
            [(ArterialEnum.BAS,)],
        ),
        (
            ArterialEnum.LAChA,
            [(ArterialEnum.LCAR,)],
        ),
        (
            ArterialEnum.RAChA,
            [(ArterialEnum.RCAR,)],
        ),
    ]
    artery_flags = {
        ArterialEnum.RPCA1.name: False,
        ArterialEnum.LPCA1.name: False,
        ArterialEnum.RPcom.name: False,
        ArterialEnum.LPcom.name: False,
        ArterialEnum.Acom.name: False,
        ArterialEnum.LACA1.name: False,
        ArterialEnum.RACA1.name: False,
        ArterialEnum.LCAR.name: False,
        ArterialEnum.RCAR.name: False,
        ArterialEnum.BAS.name: False,
        ArterialEnum.LMCA1.name: False,
        ArterialEnum.RMCA1.name: False,
        ArterialEnum.LPCA2.name: False,
        ArterialEnum.RPCA2.name: False,
        ArterialEnum.LSCA.name: False,
        ArterialEnum.RSCA.name: False,
        ArterialEnum.LAChA.name: False,
        ArterialEnum.RAChA.name: False,
    }

    for artery_marker, neighbors in artery_checks:
        centerline_markers_ends = create_patch_end_point(
            centerline_markers, artery_marker.value, step=step
        )
        if artery_marker.value in centerline_markers_present:
            bool_value = [
                np.all([np.all(centerline_markers_ends != n.value) for n in neighbor])
                for neighbor in neighbors
            ]

            if np.any(bool_value):
                logging.info(f"===> Flagging {artery_marker}")
                artery_flags[artery_marker.name] = True

        elif artery_marker.value in markers_present:
            logging.info(f"===> Flagging {artery_marker}")
            artery_flags[artery_marker.name] = True

    return artery_flags


def create_patch_end_point(
    centerline_markers: np.array, target_label: int, step: int = 1
) -> np.array:
    """Create 2 patches at each end point of the centerline artery segment end point.
       The dimension of those patches is determined by step. If step == 1, patch.shape == (3, 3, 3),
       if step == 2, patch.shape == (5, 5, 5), etc.

    Args:
        centerline_markers (np.ndarray): Centerline images of the markers
        target_label (int): Labels of the centerline markers that we want to compute the patches
        step (int): Size of the patches.

    Returns:
        centerline_markers_ends (np.array): Array containing the patches surrounding the end points of the artery segment
    """
    centerline_artery = np.zeros(centerline_markers.shape, dtype=np.float32)
    centerline_artery[centerline_markers == target_label] = 1.0
    patch_end_point = np.zeros(centerline_markers.shape, dtype=np.float32)
    indexes = np.where(centerline_markers == target_label)

    for x, y, z in zip(*indexes):

        patch = centerline_artery[x - 1 : x + 2, y - 1 : y + 2, z - 1 : z + 2]
        patch[1, 1, 1] = 0
        count = np.sum(patch)
        end_point = False
        if count in [0, 1]:
            end_point = True

        elif count == 2:
            coords1, coords2 = list(zip(*np.nonzero(patch)))
            is_end_points = []
            is_end_points.append(special_end_points(coords1, coords2, 0))
            is_end_points.append(special_end_points(coords1, coords2, 2))
            is_end_points.append(special_end_points(coords2, coords1, 0))
            is_end_points.append(special_end_points(coords2, coords1, 2))

            end_point = np.any(is_end_points)
        if end_point:
            patch_end_point[
                x - step : x + (1 + step), y - step : y + (1 + step), z - step : z + (1 + step)
            ] = 1

    centerline_markers_ends = patch_end_point * centerline_markers

    return centerline_markers_ends


def cleanup_markers(markers: np.array, voxel_volume: float, dilate: bool = False) -> np.array:
    """Do some postprocessing step to clean up the prediction

    Args:
        markers (np.ndarray): Markers prediction of the circle of willis
        voxel_volume (float): Volume used for post processing (cleaning).
        dilate (bool, optional): Dilate the model prediction. Defaults to False.

    Returns:
        cleaned_up_markers (np.array): Array containing cleaned_up markers
    """

    # Post processing
    # TODO: This part could be made on the 4D image (c, w, h, d) binarized instead of the argmax
    # version which is (w, h, d) and the content go from 0 to 14.
    logging.info("===> Volume filtering.")
    cleaned_up_markers = volume_analysis(markers, voxel_volume)
    logging.info("===> Keep largest structure.")
    cleaned_up_markers = largest_structure(
        cleaned_up_markers,
        arteries=[
            ArterialEnum.Acom.value,
            ArterialEnum.LPcom.value,
            ArterialEnum.RPcom.value,
            ArterialEnum.LPCA1.value,
            ArterialEnum.RPCA1.value,
        ],
    )
    if dilate:
        logging.info("===> Dilate segmentations.")
        cleaned_up_markers = dilation(cleaned_up_markers)

    logging.info("===> Hole filling.")
    cleaned_up_markers = remove_holes(cleaned_up_markers)
    # logging.info("===> Aplasia analysis")
    # cleaned_up_markers = aplasia_analysis(cleaned_up_markers)

    return cleaned_up_markers
