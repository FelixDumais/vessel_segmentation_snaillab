# -*- coding:utf-8 -*-

"""
University of Sherbrooke
Date:
Authors: Felix Dumais
License:
"""
import torch
import torch.nn as nn

from vessel_segmentation.NeuralNetworks.models.BaseModel import BaseModel


class ThreeDUnetDropout(BaseModel):
    """
     Class that implements the 3D Unet model from the paper
    " 3D Unet: Learning Dense Volumetric Segmentation from Sparse Annotation"
    """

    def __init__(self, num_classes=4, init_weights=False):
        """
        Build ThreeDUnetDropout model
        :param num_classes: number of possible classes
        :param init_weights: bool that activate the function _init_weights()
        """
        super(ThreeDUnetDropout, self).__init__(num_classes=num_classes)
        # encoder
        in_channels = 1  # gray image
        self.conv_encoder1 = self._contracting_block(
            in_channels=in_channels, mid_channels=32, out_channels=64
        )
        self.max_pool_encoder1 = nn.MaxPool3d(kernel_size=2)
        self.conv_encoder2 = self._contracting_block(
            in_channels=64, mid_channels=64, out_channels=128
        )
        self.max_pool_encoder2 = nn.MaxPool3d(kernel_size=2)
        self.conv_encoder3 = self._contracting_block(
            in_channels=128, mid_channels=128, out_channels=256
        )
        self.max_pool_encoder3 = nn.MaxPool3d(kernel_size=2)

        self.transitional_block = torch.nn.Sequential(
            nn.Conv3d(kernel_size=3, in_channels=256, out_channels=256, padding=1),
            nn.ReLU(),
            nn.BatchNorm3d(256),
            nn.Dropout3d(p=0.5),
            nn.Conv3d(kernel_size=3, in_channels=256, out_channels=512, padding=1),
            nn.ReLU(),
            nn.BatchNorm3d(512),
            nn.ConvTranspose3d(
                in_channels=512,
                out_channels=512,
                kernel_size=3,
                stride=2,
                padding=1,
                output_padding=1,
            ),
        )

        # Decode
        self.conv_decoder3 = self._expansive_block(
            in_channels=768, mid_channels=256, out_channels=256
        )
        self.conv_decoder2 = self._expansive_block(
            in_channels=384, mid_channels=128, out_channels=128
        )
        self.final_layer = self._final_block(
            in_channels=192, mid_channels=64, out_channels=num_classes
        )

        if init_weights:
            self._initialize_weights()

    def forward(self, x):
        """
        Forward pass of the ThreeDUnetDropout model

        :param x: tensor of shape (N, C, d1, d2, d3)
        :return: prediction score of shape (N, self.num_classes, d1, d2, d3)
        """
        # Encode
        encode_block1 = self.conv_encoder1(x)
        encode_pool1 = self.max_pool_encoder1(encode_block1)
        encode_block2 = self.conv_encoder2(encode_pool1)
        encode_pool2 = self.max_pool_encoder2(encode_block2)
        encode_block3 = self.conv_encoder3(encode_pool2)
        encode_pool3 = self.max_pool_encoder3(encode_block3)

        # Transitional block
        middle_block = self.transitional_block(encode_pool3)

        # Decode
        decode_block3 = torch.cat((middle_block, encode_block3), 1)
        cat_layer2 = self.conv_decoder3(decode_block3)
        decode_block2 = torch.cat((cat_layer2, encode_block2), 1)
        cat_layer1 = self.conv_decoder2(decode_block2)
        decode_block1 = torch.cat((cat_layer1, encode_block1), 1)
        final_layer = self.final_layer(decode_block1)
        return final_layer

    @staticmethod
    def _contracting_block(in_channels, mid_channels, out_channels, kernel_size=3):
        """
        Contracting block of the UnetDropout3D

        :param in_channels:
        :param out_channels:
        :param kernel_size:
        :return:
        """
        block = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=in_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(mid_channels),
            nn.Dropout3d(p=0.5),
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=mid_channels,
                out_channels=out_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(out_channels),
        )
        return block

    @staticmethod
    def _expansive_block(in_channels, mid_channels, out_channels, kernel_size=3):
        """
        Expansive block of the UnetDropout3D

        :param in_channels:
        :param mid_channels:
        :param out_channels:
        :param kernel_size:
        :return:
        """
        block = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=in_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(mid_channels),
            nn.Dropout3d(p=0.5),
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=mid_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(mid_channels),
            nn.ConvTranspose3d(
                in_channels=mid_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=2,
                padding=1,
                output_padding=1,
            ),
        )
        return block

    @staticmethod
    def _final_block(in_channels, mid_channels, out_channels, kernel_size=3):
        """
        Final block of the UnetDropout3D
        :param in_channels:
        :param mid_channels:
        :param out_channels:
        :param kernel_size:
        :return:
        """
        block = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=in_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(mid_channels),
            nn.Dropout3d(p=0.5),
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=mid_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.ReLU(),
            nn.BatchNorm3d(mid_channels),
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=mid_channels,
                out_channels=out_channels,
                padding=1,
            ),
        )
        return block
