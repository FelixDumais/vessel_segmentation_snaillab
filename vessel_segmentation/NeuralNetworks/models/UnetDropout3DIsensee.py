# -*- coding:utf-8 -*-

"""
University of Sherbrooke
Date:
Authors: Felix Dumais
License:
"""

import torch
import torch.nn as nn
from vessel_segmentation.NeuralNetworks.models.BaseModel import BaseModel


class UnetDropout3DIsensee(BaseModel):
    """
     Class that implements the 3D Unet model from the paper
    " 3D Unet: Learning Dense Volumetric Segmentation from Sparse Annotation"
    """

    def __init__(self, num_classes=4, init_weights=False, drop_value=0.5, channels=1):
        """
        Build ThreeDUnetDropout model
        :param num_classes: number of possible classes
        :param init_weights: bool that activate the function _init_weights()
        """
        super(UnetDropout3DIsensee, self).__init__(
            num_classes=num_classes, init_weights=init_weights
        )
        # encoder
        in_channels = 1  # gray image
        params = {"Channels": channels}
        self.drop_value = drop_value
        self.conv1 = nn.Sequential(
            nn.Conv3d(
                kernel_size=3,
                in_channels=1,
                out_channels=params["Channels"] * 16,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(params["Channels"] * 16),
        )

        self.conv_encoder1 = ContractingBlock(
            in_channels=params["Channels"] * 16,
            mid_channels=params["Channels"] * 16,
            out_channels=params["Channels"] * 16,
            dropout=self.drop_value,
        )

        self.pool_encoder1 = nn.Conv3d(
            kernel_size=3,
            stride=2,
            in_channels=params["Channels"] * 16,
            out_channels=params["Channels"] * 32,
            padding=1,
        )

        self.conv_encoder2 = ContractingBlock(
            in_channels=params["Channels"] * 32,
            mid_channels=params["Channels"] * 32,
            out_channels=params["Channels"] * 32,
            dropout=self.drop_value,
        )

        self.pool_encoder2 = nn.Conv3d(
            kernel_size=3,
            stride=2,
            in_channels=params["Channels"] * 32,
            out_channels=params["Channels"] * 64,
            padding=1,
        )

        self.conv_encoder3 = ContractingBlock(
            in_channels=params["Channels"] * 64,
            mid_channels=params["Channels"] * 64,
            out_channels=params["Channels"] * 64,
            dropout=self.drop_value,
        )

        self.pool_encoder3 = nn.Conv3d(
            kernel_size=3,
            stride=2,
            in_channels=params["Channels"] * 64,
            out_channels=params["Channels"] * 128,
            padding=1,
        )

        self.conv_encoder4 = ContractingBlock(
            in_channels=params["Channels"] * 128,
            mid_channels=params["Channels"] * 128,
            out_channels=params["Channels"] * 128,
            dropout=self.drop_value,
        )

        self.pool_encoder4 = nn.Conv3d(
            kernel_size=3,
            stride=2,
            in_channels=params["Channels"] * 128,
            out_channels=params["Channels"] * 256,
            padding=1,
        )

        self.transitional_block = ContractingBlock(
            in_channels=params["Channels"] * 256,
            mid_channels=params["Channels"] * 256,
            out_channels=params["Channels"] * 256,
            dropout=self.drop_value,
        )

        self.upsample4 = nn.Sequential(
            nn.Conv3d(
                kernel_size=1,
                stride=1,
                in_channels=params["Channels"] * 256,
                out_channels=params["Channels"] * 128,
                padding=0,
            ),
            nn.Upsample(size=12, scale_factor=None, mode="trilinear", align_corners=True),
        )
        self.conv_decoder4 = ExpansiveBlock(
            in_channels=params["Channels"] * 256,
            mid_channels=params["Channels"] * 256,
            out_channels=params["Channels"] * 128,
        )

        self.upsample3 = nn.Sequential(
            nn.Conv3d(
                kernel_size=1,
                stride=1,
                in_channels=params["Channels"] * 128,
                out_channels=params["Channels"] * 64,
                padding=0,
            ),
            nn.Upsample(size=24, scale_factor=None, mode="trilinear", align_corners=True),
        )
        self.conv_decoder3 = ExpansiveBlock(
            in_channels=params["Channels"] * 128,
            mid_channels=params["Channels"] * 128,
            out_channels=params["Channels"] * 64,
        )

        self.conv_segmentation1 = nn.Conv3d(
            kernel_size=1,
            in_channels=params["Channels"] * 64,
            out_channels=self.num_classes,
            padding=0,
        )

        self.upsample_segmentation1 = nn.Upsample(
            size=48, scale_factor=None, mode="trilinear", align_corners=True
        )

        self.upsample2 = nn.Sequential(
            nn.Conv3d(
                kernel_size=1,
                stride=1,
                in_channels=params["Channels"] * 64,
                out_channels=params["Channels"] * 32,
                padding=0,
            ),
            nn.Upsample(size=48, scale_factor=None, mode="trilinear", align_corners=True),
        )
        self.conv_decoder2 = ExpansiveBlock(
            in_channels=params["Channels"] * 64,
            mid_channels=params["Channels"] * 64,
            out_channels=params["Channels"] * 32,
        )
        self.conv_segmentation2 = nn.Conv3d(
            kernel_size=1,
            in_channels=params["Channels"] * 32,
            out_channels=self.num_classes,
            padding=0,
        )

        self.upsample_segmentation2 = nn.Upsample(
            size=96, scale_factor=None, mode="trilinear", align_corners=True
        )
        self.upsample1 = nn.Sequential(
            nn.Conv3d(
                kernel_size=1,
                stride=1,
                in_channels=params["Channels"] * 32,
                out_channels=params["Channels"] * 16,
                padding=0,
            ),
            nn.Upsample(size=96, scale_factor=None, mode="trilinear", align_corners=True),
        )
        self.conv_final = nn.Sequential(
            nn.Conv3d(
                kernel_size=3,
                in_channels=params["Channels"] * 32,
                out_channels=params["Channels"] * 32,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(params["Channels"] * 32),
        )
        self.final = nn.Conv3d(
            kernel_size=3,
            in_channels=params["Channels"] * 32,
            out_channels=self.num_classes,
            padding=1,
        )

        if self.init_weights:
            self.conv1.apply(self.weights_init2)
            self.conv_encoder1.apply(self.weights_init2)
            self.conv_encoder2.apply(self.weights_init2)
            self.conv_encoder3.apply(self.weights_init2)
            self.conv_encoder4.apply(self.weights_init2)
            self.transitional_block.apply(self.weights_init2)
            self.conv_decoder4.apply(self.weights_init2)
            self.conv_decoder3.apply(self.weights_init2)
            self.conv_decoder2.apply(self.weights_init2)
            self.conv_final.apply(self.weights_init2)

            self.pool_encoder1.apply(self.weights_init1)
            self.pool_encoder2.apply(self.weights_init1)
            self.pool_encoder3.apply(self.weights_init1)
            self.pool_encoder4.apply(self.weights_init1)
            self.upsample4.apply(self.weights_init1)
            self.upsample3.apply(self.weights_init1)
            self.upsample2.apply(self.weights_init1)
            self.upsample1.apply(self.weights_init1)
            self.conv_segmentation1.apply(self.weights_init1)
            self.conv_segmentation2.apply(self.weights_init1)
            self.final.apply(self.weights_init1)

    def weights_init1(self, m):
        """Weights initialization function

        Args:
            m: Model modules

        Returns:

        """

        if isinstance(m, nn.Conv3d):
            nn.init.xavier_normal_(m.weight, gain=1)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)

        elif isinstance(m, nn.BatchNorm3d):
            nn.init.constant_(m.weight, 1)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)

    def weights_init2(self, m):
        """Weights initialization function

        Args:
            m: Model modules

        Returns:

        """

        if isinstance(m, nn.Conv3d):
            nn.init.kaiming_normal_(m.weight, mode="fan_out", nonlinearity="leaky_relu")

            if m.bias is not None:
                nn.init.constant_(m.bias, 0)

        elif isinstance(m, nn.BatchNorm3d):
            nn.init.constant_(m.weight, 1)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)

    def forward(self, x):
        """
        Forward pass of the ThreeDUnetDropout model

        :param x: tensor of shape (N, C, d1, d2, d3)
        :return: prediction score of shape (N, self.num_classes, d1, d2, d3)
        """
        # Encode
        conv1 = self.conv1(x)
        encode_block1 = self.conv_encoder1(conv1)
        encode_pool1 = self.pool_encoder1(encode_block1)
        encode_block2 = self.conv_encoder2(encode_pool1)
        encode_pool2 = self.pool_encoder2(encode_block2)
        encode_block3 = self.conv_encoder3(encode_pool2)
        encode_pool3 = self.pool_encoder3(encode_block3)
        encode_block4 = self.conv_encoder4(encode_pool3)
        encode_pool4 = self.pool_encoder4(encode_block4)

        middle_block = self.transitional_block(encode_pool4)

        upsample4 = self.upsample4(middle_block)
        decode_block4 = torch.cat((encode_block4, upsample4), 1)
        cat_layer3 = self.conv_decoder4(decode_block4)

        upsample3 = self.upsample3(cat_layer3)
        decode_block3 = torch.cat((encode_block3, upsample3), 1)
        cat_layer2 = self.conv_decoder3(decode_block3)

        upsample2 = self.upsample2(cat_layer2)
        decode_block2 = torch.cat((encode_block2, upsample2), 1)
        cat_layer1 = self.conv_decoder2(decode_block2)

        upsample1 = self.upsample1(cat_layer1)
        decode_block1 = torch.cat((encode_block1, upsample1), 1)
        final = self.conv_final(decode_block1)
        final = self.final(final)

        segmentation1 = self.conv_segmentation1(cat_layer2)
        segmentation1 = self.upsample_segmentation1(segmentation1)

        segmentation2 = self.conv_segmentation2(cat_layer1)

        segmentation2 = segmentation1 + segmentation2

        segmentation2 = self.upsample_segmentation2(segmentation2)
        final = final + segmentation2

        return final


class ContractingBlock(nn.Module):
    def __init__(self, in_channels, mid_channels, out_channels, kernel_size=3, dropout=0.5):
        super(ContractingBlock, self).__init__()

        self.block1 = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=in_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(mid_channels),
        )

        self.dropout_block = nn.Dropout3d(p=dropout)

        self.block2 = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=mid_channels,
                out_channels=out_channels,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(out_channels),
        )

    def forward(self, x):
        out = self.block1(x)
        out = self.dropout_block(out)
        out = self.block2(out)

        assert x.shape == out.shape
        out = out + x

        return out


class ExpansiveBlock(nn.Module):
    def __init__(self, in_channels, mid_channels, out_channels, kernel_size=3):
        super(ExpansiveBlock, self).__init__()

        self.block1 = nn.Sequential(
            nn.Conv3d(
                kernel_size=kernel_size,
                in_channels=in_channels,
                out_channels=mid_channels,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(mid_channels),
        )

        self.block2 = nn.Sequential(
            nn.Conv3d(
                kernel_size=1,
                in_channels=mid_channels,
                out_channels=out_channels,
                padding=0,
            ),
            nn.LeakyReLU(),
            nn.BatchNorm3d(out_channels),
        )

    def forward(self, x):
        mid = self.block1(x)
        out = self.block2(mid)

        return out
