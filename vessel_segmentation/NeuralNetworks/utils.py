# -*- coding: utf-8 -*-
import numpy as np
from skimage.util import crop
from skimage.transform import resize
from typing import Tuple, Union


def pad_axis(image: np.array, axis: int, padding: int, ndim: int) -> np.array:
    """Function to center pad an image

    Args:
        image (np.array): 3D or 4D image to pad
        pad_size (int): Size of the padding
        dim (int): Dimension to pad
        c_val (float): Value to use in the padding
        ndim (int): Number of dimensions of the image

    Returns:
        (np.array): padded image

    """

    side_pad = padding // 2

    if padding % 2 == 0:
        if ndim == 3:
            if axis == 0:
                image = np.pad(image, ((side_pad,), (0,), (0,)), mode="constant")
            elif axis == 1:
                image = np.pad(image, ((0,), (side_pad,), (0,)), mode="constant")
            elif axis == 2:
                image = np.pad(image, ((0,), (0,), (side_pad,)), mode="constant")
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")
        elif ndim == 4:
            if axis == 0:
                image = np.pad(image, ((0,), (side_pad,), (0,), (0,)), mode="constant")
            elif axis == 1:
                image = np.pad(image, ((0,), (0,), (side_pad,), (0,)), mode="constant")
            elif axis == 2:
                image = np.pad(image, ((0,), (0,), (0,), (side_pad,)), mode="constant")
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")

    else:
        if ndim == 3:
            if axis == 0:
                image = np.pad(image, ((side_pad, side_pad + 1), (0, 0), (0, 0)), mode="constant")
            elif axis == 1:
                image = np.pad(image, ((0, 0), (side_pad, side_pad + 1), (0, 0)), mode="constant")
            elif axis == 2:
                image = np.pad(image, ((0, 0), (0, 0), (side_pad, side_pad + 1)), mode="constant")
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")

        elif ndim == 4:
            if axis == 0:
                image = np.pad(
                    image, ((0, 0), (side_pad, side_pad + 1), (0, 0), (0, 0)), mode="constant"
                )
            elif axis == 1:
                image = np.pad(
                    image, ((0, 0), (0, 0), (side_pad, side_pad + 1), (0, 0)), mode="constant"
                )
            elif axis == 2:
                image = np.pad(
                    image, ((0, 0), (0, 0), (0, 0), (side_pad, side_pad + 1)), mode="constant"
                )
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")

    return image


def crop_axis(image: np.array, axis: int, crop: int, ndim: int) -> np.array:
    """Function that crop numpy array

    Args:
        image (np.array): Image to crop
        axis (int): Axis to crop
        crop (int): Number of voxel to crop

    Returns:
        image (np.array): Cropped image

    """
    side_crop = crop // 2

    if crop % 2 == 0:
        if ndim == 3:
            if axis == 0:
                image = image[side_crop:-side_crop, ...]
            elif axis == 1:
                image = image[:, side_crop:-side_crop, ...]
            elif axis == 2:
                image = image[:, :, side_crop:-side_crop]
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")
        elif ndim == 4:
            if axis == 0:
                image = image[:, side_crop:-side_crop, ...]
            elif axis == 1:
                image = image[:, :, side_crop:-side_crop, ...]
            elif axis == 2:
                image = image[:, :, :, side_crop:-side_crop]
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")

    else:
        if ndim == 3:
            if axis == 0:
                image = image[side_crop : -(side_crop + 1), ...]
            elif axis == 1:
                image = image[:, side_crop : -(side_crop + 1), ...]
            elif axis == 2:
                image = image[:, :, side_crop : -(side_crop + 1)]
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")
        elif ndim == 4:
            if axis == 0:
                image = image[:, side_crop : -(side_crop + 1), ...]
            elif axis == 1:
                image = image[:, :, side_crop : -(side_crop + 1), ...]
            elif axis == 2:
                image = image[:, :, :, side_crop : -(side_crop + 1)]
            else:
                raise ValueError("Axis value accepted are 0, 1 or 2")

    return image


def resize_image(
    image: np.array,
    shape: Union[Tuple[int, int, int], int] = 96,
) -> np.array:
    """Centered image resize using crop or padding with c_val.

    Args:
        image (np.array): a 3D or 4D image (C, d1, d2, d3).
        size (Union[Tuple[int, int, int], int]): shape of the image
        c_val (float): value used to pad

    Returns:
        image (np.array): image center resized

    """

    ndim = image.ndim
    assert 3 <= ndim <= 4

    if ndim == 3:
        x, y, z = image.shape
    else:
        c, x, y, z = image.shape

    if not isinstance(shape, tuple):
        shape = (shape, shape, shape)

    x_padding = shape[0] - x
    y_padding = shape[1] - y
    z_padding = shape[2] - z

    # Check the first dimension to select if we crop of pad
    if x_padding > 0:
        image = pad_axis(image, 0, x_padding, ndim)
    elif x_padding < 0:
        image = crop_axis(image, 0, -x_padding, ndim)

    if y_padding > 0:
        image = pad_axis(image, 1, y_padding, ndim)
    elif y_padding < 0:
        image = crop_axis(image, 1, -y_padding, ndim)

    if z_padding > 0:
        image = pad_axis(image, 2, z_padding, ndim)
    elif z_padding < 0:
        image = crop_axis(image, 2, -z_padding, ndim)

    if ndim == 3:
        assert image.shape == shape
    else:
        assert image.shape == tuple(
            [
                c,
            ]
            + list(shape)
        )

    return image
