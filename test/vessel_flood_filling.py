#!/usr/bin/env python3
"""
Script to generate vasculature on whole brain.
"""
import argparse
from typing import Tuple

import nibabel as nib
import numpy as np
from vessel_segmentation.prediction import dipy_nlmeans
from vessel_segmentation.tof_master import (
    extract_vessels_ved,
    hysteresis_thresholding_brain,
    extend_markers,
    correct_watershed,
    apply_transform,
)


def vessels_flood_filling(
    markers_data: np.array,
    resample_array: np.array,
    cube: np.array,
    mask: np.array,
    affine: np.array,
    header: nib.Nifti1Header,
    prefix: str,
    ss_mask: str,
    resample_file: str,
    transform: str,
) -> Tuple[np.array, np.array]:
    """Watershed markers in the binary vessel image

    Args:
        markers_data (np.array): Circle of Willis.
        resample_array (np.array): Raw data resampled.
        cube (np.array): Binary cube.
        mask (np.array): Brain mask.
        affine (np.array): Affine matrix.
        header (nib.Nifti1Header): Header.
        prefix (str): File name prefix.
        ss_mask (str): Skull Stripped mask.
        resample_file (str): TOF resampled.
        transform (str): ANTS affine registration matrix file.

    Returns:
        labels (np.array): Watershed image.
    """

    ss_mask_reg = apply_transform(
        ss_mask,
        resample_file,
        transform,
        f"{prefix}__SSS_masked_reg.nii.gz",
    )
    vesselness_file = f"{prefix}__vesselness.nii.gz"
    vesselness_nlmeans_file = f"{prefix}__vesselness_nlmeans.nii.gz"
    vessels_file = f"{prefix}__vessels_hysteresis.nii.gz"
    labels_file = f"{prefix}__labels.nii.gz"
    resample_file_scaled = f"{prefix}__resampled_scaled.nii.gz"

    resample_array_non_zero = resample_array[markers_data != 0]
    upper_quantile = np.quantile(resample_array_non_zero, 0.9)
    resample_array /= upper_quantile

    nib.Nifti1Image(resample_array, affine, header).to_filename(resample_file_scaled)

    # Create a mask with the skullstripped mask and the cube with Sagittial Sinus removed
    mask_cube = np.logical_or(mask, cube).astype(float) - ss_mask_reg

    vesselness = extract_vessels_ved(
        resample_file_scaled, vesselness_file, mask_cube, ".", move=False
    )
    nib.Nifti1Image(vesselness, affine, header).to_filename(vesselness_file)

    vesselness_nlmeans = dipy_nlmeans(vesselness, mask=mask_cube)
    nib.Nifti1Image(vesselness_nlmeans, affine, header).to_filename(vesselness_nlmeans_file)

    vessels = hysteresis_thresholding_brain(vesselness_nlmeans, mask_cube)
    nib.Nifti1Image(vessels, affine, header).to_filename(vessels_file)

    labels = extend_markers(vessels, markers_data)
    labels = correct_watershed(markers_data, labels)
    nib.Nifti1Image(labels, affine, header).to_filename(labels_file)

    return labels


def command_line_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="""Generate a segmentation of the vessels in the brain.

        Ref: Bernier, M., Cunnane, S. C., & Whittingstall, K. (2018).
        The morphology of the human cerebrovascular system.
        Human Brain Mapping. https://doi.org/10.1002/hbm.24337
        """,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("tof", help="Time-Of-Flight mri modality.")
    parser.add_argument("cube", help="Segmentaion cube containing the circle of willis.")
    parser.add_argument("mask", help="Brain mask.")
    parser.add_argument("markers", help="Arterial markers of the circle of willis.")
    parser.add_argument("sss_masked", help="Superior sagittal sinus template image.")
    parser.add_argument("transform", help="Affine matrix for registration.")
    parser.add_argument("prefix", help="Prefix to use when saving files.")

    return parser.parse_args()


def main():
    args = command_line_arguments()
    tof = nib.load(args.tof)
    cube = nib.load(args.cube)
    mask = nib.load(args.mask)
    markers = nib.load(args.markers)

    assert tof.shape == markers.shape, "TOF and Markers shape mismatch"
    assert tof.shape == cube.shape, "TOF and Cube shape mismatch"
    assert tof.shape == mask.shape, "TOF and Mask shape mismatch"

    affine: np.ndarray = tof.affine
    header: nib.Nifti1Header = tof.header

    vessels_flood_filling(
        markers.get_fdata(),
        tof.get_fdata(),
        cube.get_fdata(),
        mask.get_fdata(),
        affine,
        header,
        args.prefix,
        args.sss_masked,
        args.tof,
        args.transform,
    )


if __name__ == "__main__":
    main()
