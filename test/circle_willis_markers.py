#!/usr/bin/env python3
"""
Segmentation of the circle of willis script.
TODO: Add paper reference.
"""
import argparse
import logging
import nibabel as nib
import numpy as np
import torch
from vessel_segmentation.postprocessing import cleanup_markers
from vessel_segmentation.prediction import majority_vote, dipy_nlmeans, predict_file
from vessel_segmentation.tof_master import hysteresis_thresholding_cube, extract_vessels_itk


def command_line_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Segmentation of the circle of willis.")
    parser.add_argument("image", help="Input image to extract the circle of willis.")
    parser.add_argument("cube", help="Segmentation cube containing the circle of willis.")
    parser.add_argument("prefix", help="Prefix to use when saving files.")
    parser.add_argument("--raw_model", required=True, help="Model segmentation on the raw image.")
    parser.add_argument(
        "--vessel_model", required=True, help="Model segmentation on the vessel image."
    )
    parser.add_argument(
        "--nlmeans_model", required=True, help="Model segmentation on the nlmeans image."
    )
    parser.add_argument(
        "--binary_model", required=True, help="Model segmentation on the binary image."
    )
    parser.add_argument("--device", default="cpu", help="Choose the inference device (cpu|cuda).")
    parser.add_argument(
        "--num_threads", default=1, type=int, help="Number of threads to use in pytorch."
    )
    return parser.parse_args()


def main():
    args = command_line_arguments()
    torch.set_num_threads(args.num_threads)
    if args.device == "gpu" and not torch.cuda.is_available():
        logging.warning(
            "GPU not available in pytorch, check your configuration. Device set to CPU."
        )
        device = torch.device("cpu")
    else:
        device = torch.device(args.device)

    resample = nib.load(args.image)
    cube = nib.load(args.cube)
    voxel_volume = 0.625 ** 3

    resample_data = resample.get_fdata("unchanged")
    cube_data = cube.get_fdata("unchanged")
    binary_image = hysteresis_thresholding_cube(
        resample_data, cube_data, voxel_size=0.625, method=2
    )
    nib.Nifti1Image(binary_image, resample.affine, resample.header).to_filename(
        f"{args.prefix}__bin_vessels.nii.gz"
    )

    kwargs = dict(
        output="",
        prefix=args.prefix,
        cube=cube_data,
        binary_image=binary_image,
        voxel_volume=voxel_volume,
        nii_attributes=resample,
        device=device,
        no_attention=False,
    )

    probabilities = []
    _, probability = predict_file(type="raw", model=args.raw_model, array=resample_data, **kwargs)
    probabilities.append(probability)

    vesselness = extract_vessels_itk(args.image, f"{args.prefix}__vessels.nii.gz")
    _, probability = predict_file(
        type="vessel", model=args.vessel_model, array=vesselness, **kwargs
    )
    probabilities.append(probability)

    nlmeans_image = dipy_nlmeans(resample_data)
    nib.Nifti1Image(nlmeans_image, resample.affine, resample.header).to_filename(
        f"{args.prefix}__nlmeans.nii.gz"
    )
    _, probability = predict_file(
        type="nlmeans", model=args.nlmeans_model, array=nlmeans_image, **kwargs
    )
    probabilities.append(probability)

    _, probability = predict_file(
        type="binary", model=args.binary_model, array=binary_image, **kwargs
    )
    probabilities.append(probability)

    markers = majority_vote(probabilities)
    markers = cleanup_markers(markers, voxel_volume)

    nib.Nifti1Image(markers, resample.affine, resample.header).to_filename(
        f"{args.prefix}__markers.nii.gz"
    )


if __name__ == "__main__":
    main()
