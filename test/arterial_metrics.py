#!/usr/bin/env python3
"""
Script to compute the arterial metrics.
"""
import argparse
from collections import OrderedDict
import logging
from typing import Sequence, Tuple

import nibabel as nib
import numpy as np
import pandas as pd
from vessel_segmentation import ARTERIAL_MARKER_LIST
from vessel_segmentation.arterial_vessel import (
    analyze_skeleton,
    compute_vascular_distance,
    diameter_transform,
    generate_frequency_path,
    find_skeleton_paths,
    find_start_point,
    find_true_skeleton,
    largest_structure,
)
from vessel_segmentation.postprocessing import centerline_analysis
from vessel_segmentation.tof_master import (
    centerline_transform,
    edt_transform,
    find_willis_center,
)


def extract_centerline_and_diameter(
    tof: nib.Nifti1Image,
    markers_bin: np.ndarray,
    prefix: str,
    edt: bool,
    brain_vasculature: str = "CircleOfWillis",
) -> Tuple[np.ndarray, np.ndarray]:
    """Extract center line and diameter from markers mask.

    Args:
        tof (nib.Nifti1Image): Time of flight image.
        markers_bin (np.ndarray): Markers result of the segmentation.
        prefix (str): File name prefix.
        edt (bool): Enable edt transform instead of diameter transform.
        brain_vasculature (str, optional): Defaults to "CircleOfWillis".

    Returns:
        Tuple[np.ndarray, np.ndarray]: center line and diameter images.
    """
    logging.info("4. ===> Arterial Centerline & Diameter <===")
    logging.info("   4.1 ++++ : Extracting Centerline")
    center_line = centerline_transform(markers_bin)
    nib.Nifti1Image(center_line, tof.affine, tof.header).to_filename(
        f"{prefix}_{brain_vasculature}_centerline.nii.gz"
    )

    logging.info("   4.2 ++++ : Extracting Diameter")
    if edt:
        diameter = edt_transform(markers_bin, center_line, tof.header.get_zooms())
    else:
        diameter = diameter_transform(markers_bin, tof.header.get_zooms())

    nib.Nifti1Image(diameter, tof.affine, tof.header).to_filename(
        f"{prefix}_{brain_vasculature}_diameter.nii.gz"
    )

    return center_line, diameter


def compute_arterial_metrics(
    markers_data: np.ndarray,
    center_line: np.ndarray,
    diameter: np.ndarray,
    center: Sequence[float],
    affine: np.ndarray,
    header: nib.Nifti1Header,
    prefix: str,
):
    """Compute simple metrics within the markers and output SUMMARY csv file

    Args:
        markers_data (np.ndarray): Markers segmentation.
        center_line (np.ndarray): Center line image of the markers
        diameter (np.ndarray): Diameter image of the markers
        center (Sequence[float]): Center of mass of the markers
        affine (np.ndarray): Reference affine.
        header (nib.Nifti1Header): Reference header.
        prefix (str): Use as prefix of filename.
        brain_vasculature (str, optional): Defaults to "CircleOfWillis".
    """

    logging.info("3. ===> Computing Arterial Metrics <===")

    statistics = OrderedDict(
        (
            ("Volume", np.nan),
            ("Diameter mean", np.nan),
            ("Diameter median", np.nan),
            ("Diameter max", np.nan),
            ("Diameter min", np.nan),
            ("Diameter STD", np.nan),
            ("Diameter N", np.nan),
            ("Weighted average tortuosity", np.nan),
            ("Angulation STD", np.nan),
            ("Total length", np.nan),
            ("Longest path", np.nan),
            ("Confidence", "Bad"),
        )
    )

    metrics = OrderedDict([(elem, statistics.copy()) for elem in ARTERIAL_MARKER_LIST[1:]])
    center_line_diameter = center_line * diameter

    artery_flags = centerline_analysis(markers_data, center_line, step=1)
    mean_zoom_cube = np.mean(header.get_zooms()) ** 3
    for label in np.unique(markers_data)[1:]:
        if label > len(ARTERIAL_MARKER_LIST):
            logging.warning(f"Label {label} isn't an identified artery!")
            continue

        artery_prefix = ARTERIAL_MARKER_LIST[int(label)]
        path_prefix = f"{prefix}__CircleOfWillis_{artery_prefix}"

        logging.info(f"++++ Starting {path_prefix} Artery Metrics ++++")

        roi = np.zeros_like(markers_data)
        roi[markers_data == label] = 1

        logging.info("5.1 ++++ Largest Structure Correction ++++")
        structure = largest_structure(roi)
        nib.Nifti1Image(structure, affine, header).to_filename(f"{path_prefix}_largest.nii.gz")

        metrics[artery_prefix]["Volume"] = np.count_nonzero(structure) * mean_zoom_cube
        seg_diameter = center_line_diameter * structure
        seg_diameter: np.ndarray = seg_diameter[seg_diameter != 0].flatten()

        logging.info("5.2 ++++ Diameter Extraction ++++")
        if seg_diameter.size != 0:
            logging.info("   ===> DIAMETERS:")
            metrics[artery_prefix]["Diameter mean"] = seg_diameter.mean()
            metrics[artery_prefix]["Diameter median"] = np.median(seg_diameter)
            metrics[artery_prefix]["Diameter max"] = seg_diameter.max()
            metrics[artery_prefix]["Diameter min"] = seg_diameter.min()
            metrics[artery_prefix]["Diameter STD"] = seg_diameter.std()
            metrics[artery_prefix]["Diameter N"] = seg_diameter.size
            metrics[artery_prefix]["Confidence"] = "Good"

        logging.info("5.3 ++++ Analyzing Skeleton ++++")

        skeleton = analyze_skeleton(structure * center_line)
        nib.Nifti1Image(skeleton, affine, header).to_filename(f"{path_prefix}_analysed_skl.nii.gz")

        # Determining if a single end point is found in the current artery
        unique, counts = np.unique(skeleton, return_counts=True)

        skl_dict = dict(zip(unique[1:], counts[1:]))
        if 9 not in skl_dict or (skl_dict[9] < 2 and 7 not in skl_dict):
            logging.info(f"Artery {path_prefix} has insufficient end tags for analysis")
            metrics[artery_prefix]["Confidence"] = "Bad"
            continue

        if skl_dict[9] == 1 and skl_dict[7] >= 1:
            logging.info(f"Artery {path_prefix} Has Only One End Tags and One Junction tags")
            logging.info("    ===> Converting Junction Tag To End Tag")
            skeleton[skeleton == 7] = 9

        logging.info("5.4 ++++ Finding Willis Start Point ++++")

        start = find_start_point(skeleton, center)

        logging.info("5.5 ++++ Djikstra's Skeleton Paths Computation ++++")

        _, indices = find_skeleton_paths(start, skeleton)

        logging.info("5.6 ++++ Computing True Skeleton ++++")

        true_skeleton = find_true_skeleton(indices, skeleton.shape)
        nib.Nifti1Image(true_skeleton, affine, header).to_filename(f"{path_prefix}_skl_true.nii.gz")

        logging.info("5.7 ++++ Computing Frequency Path ++++")

        frequency = generate_frequency_path(indices, skeleton.shape)

        nib.Nifti1Image(frequency, affine, header).to_filename(
            f"{path_prefix}_skl_frequency.nii.gz"
        )

        logging.info(
            "5.8 ++++ Computing Vascular Distance & Mask Distance to Vessel & Travelled ++++"
        )
        (
            distance,
            tortuosity,
            angulation,
            corrected_length,
            average_tortuosity,
        ) = compute_vascular_distance(roi, indices, frequency, header.get_zooms())
        nib.Nifti1Image(distance, affine, header).to_filename(f"{path_prefix}_skl_distance.nii.gz")
        nib.Nifti1Image(tortuosity, affine, header).to_filename(
            f"{path_prefix}_skl_tortuosity.nii.gz"
        )
        nib.Nifti1Image(angulation, affine, header).to_filename(
            f"{path_prefix}_skl_angulation.nii.gz"
        )

        logging.info("5.9 ++++ Distance Wise Table Computation ++++")
        metrics[artery_prefix]["Weighted average tortuosity"] = average_tortuosity
        metrics[artery_prefix]["Angulation STD"] = angulation.std()
        metrics[artery_prefix]["Total length"] = corrected_length
        metrics[artery_prefix]["Longest path"] = distance.max()

        if artery_prefix in artery_flags.keys():
            if artery_flags[artery_prefix]:
                metrics[artery_prefix]["Confidence"] = "Bad"

        elif metrics[artery_prefix]["Confidence"] != "Bad":
            metrics[artery_prefix]["Confidence"] = "Good"

    pd.DataFrame(metrics).to_csv(f"{prefix}__SUMMARY.csv")


def command_line_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Compute arterial metrics from TOF and markers.")
    parser.add_argument("tof", help="Time-Of-Flight mri modality.")
    parser.add_argument("cube", help="Cube containing the circle of willis.")
    parser.add_argument("mask", help="Brain mask")
    parser.add_argument("markers", help="Circle of willis arterial markers.")
    parser.add_argument("prefix", help="Prefix to use when saving files.")
    parser.add_argument(
        "--edt", action="store_true", help="Change the euclidean distance calculation for diameter."
    )

    return parser.parse_args()


def main():
    args = command_line_arguments()
    tof = nib.load(args.tof)
    cube = nib.load(args.cube)
    mask = nib.load(args.mask)
    markers = nib.load(args.markers)

    assert tof.shape == markers.shape, "TOF and Markers shape mismatch"
    assert tof.shape == cube.shape, "TOF and Cube shape mismatch"
    assert tof.shape == mask.shape, "TOF and Mask shape mismatch"

    affine: np.ndarray = tof.affine
    header: nib.Nifti1Header = tof.header
    center, markers_data = find_willis_center(markers.get_fdata())

    centerline, diameter = extract_centerline_and_diameter(tof, markers_data, args.prefix, args.edt)

    compute_arterial_metrics(
        markers.get_fdata(), centerline, diameter, center, affine, header, args.prefix
    )


if __name__ == "__main__":
    main()
